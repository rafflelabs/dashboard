$(document).ready(function() {
    app.init();

});

/**
 * Base app object
 * @type {{init: Function}}
 */
app = {
    init: function() {
        item.create_btn_init('.js_create');
        item.delete_btn_init('.js_delete');

        ui.data_tables_init();
        ui.dependent_selects_init();
        ui.date_picker_init();
        ui.checkbox_init();
        ui.editor_init('.js-editor');
        ui.box_collapse();

        forms.submit_init('.js-submit');
        forms.submit_file_init('.js-file-init');

        media.select('.js-media-select');
        media.uploadPreview('.js-input-preview');
        media.checkAllBtnInit('.js-media-checkAll');
    }
};

/**
 * Simple action for creating
 * @type {{create_item_btns_init: Function, add_item: Function}}
 */

item = {
    create: function() {
        location.reload();
    },
    delete: function(item_tag) {
        $(item_tag).remove();
    },
    create_btn_init: function(btn) {
        $('body').on('click', btn, function(e) {
            e.preventDefault();
            var action = $(this).data('action');

            var show_modal = function(content) {
                modals.withFrom(item.create, content);
            };

            controls.ajax_action(action, 'GET', show_modal);
        })
    },
    delete_btn_init: function(btn) {
        $('body').on('click', btn, function(event) {
            event.preventDefault();
            var action = $(this).data('request');
            var removing_item_tag = $(this).data('item');
            var method = $(this).data('method');

            method = typeof method !== 'undefined' ? method : 'POST';

            var remove_item = function() {
                item.delete(removing_item_tag);
            };

            var show_error = function() {
                $('.status').html(data.responseText);
            };

            var action_function = function() {
                controls.ajax_action(action, method, remove_item, show_error);
            };

            modals.base(action_function, 'Подтвердите удаление', 'Объект будет удален', 'Удалить');
        })
    }
}

modals = {
    /**
     * Open modal for action that need confirmation
     *
     * @param confirm_function Would be call after confirm
     * @param title
     * @param content
     * @param btn_name
     */
    base: function(confirm_function, title, content, btn_name) {
        var modal_blk = $('.js_base_modal');
        this.set_content(modal_blk, title, content, btn_name);

        $(modal_blk).on('click', '.js_confirm_btn', function() {
            confirm_function();
            $(modal_blk).modal('hide');
            $(modal_blk).off('click', '.js_confirm_btn');
        });

        $(modal_blk).modal();
    },

    /**
     * Open modal with form
     *
     * @param confirm_function
     * @param content
     */
    withFrom: function(confirm_function, content) {
        var modal_blk = $('.js_empty_modal');
        $(modal_blk).html(content);
        ui.date_picker_init();

        var success_function = function(data) {
            confirm_function(data);
            $(modal_blk).modal('hide');
        };

        var error_function = function(errors) {
            errors = $.parseJSON(errors);
            var errors_string = '';
            $.map(errors, function(val, name) {
                errors_string += name + ': ' + val + '<br>';
                $(modal_blk).find('.status').html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Возникли ошибки</h4>' + errors_string + '</div>');
            });
        };

        var action = $(modal_blk).find('form').attr('action');
        var method = $(modal_blk).find('form').attr('method');

        $(modal_blk).on('submit', 'form', function(event) {
            event.preventDefault();
            var data = $(this).serialize();
            controls.ajax_action(action, method, success_function, error_function, data);
        });

        $(modal_blk).on('hidden.bs.modal', function() {
            $(modal_blk).off('submit', 'form');
        });

        $(modal_blk).modal();

    },


    /**
     * Set modal content
     *
     * @param modal_blk
     * @param title
     * @param content
     * @param btn_name
     */
    set_content: function(modal_blk, title, content, btn_name) {
        $(modal_blk).find('.modal-title').html(title);
        $(modal_blk).find('.modal-body').find('p').html(content);
        $(modal_blk).find('.js_confirm_btn').html(btn_name);
    }

};

alert = {
    /**
     * Show success alert
     * @param title
     * @param txt
     */
    success: function(title, txt) {
        var el = $('.alert-section');
        var alertHtml = '<div class="alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4 class="ttl"><i class="icon fa fa-check"></i>' + title + '</h4> <span class="txt">' + txt + '</span> </div>';
        var alertItem = el.append(alertHtml);
        $('.alert-success').slideDown();
        setTimeout(function() {
            $('.alert-success').first().slideUp(500, function() {
                $(this).remove();
            });
        }, 3000)
    },
    /**
     * Show error alert
     * @param title
     * @param txt
     */
    error: function(title, txt) {
        var el = $('.alert-section');
        var alertHtml = '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4 class="ttl"><i class="icon fa fa-ban"></i>' + title + '</h4> <span class="txt">' + txt + '</span> </div>';
        var alertItem = el.append(alertHtml);
        $('.alert-danger').slideDown();
    }
}

forms = {
    submit_init: function(form) {
        $('body').on('submit', form, function(e) {
            var form = this;
            var btn = $(form).find('button');
            var method = ($(form).attr('method') == undefined ? "POST" : $(form).attr('method'));
            var action = ($(form).attr('action') == undefined ? "" : $(form).attr('action'));
            e.preventDefault();
            var data = $(form).serialize();
            var succees = function() {
                alert.success('', 'Данные успешно сохранены.');
            }
            var error = function() {
                alert.error('Ошибка!', 'Попробуйте ещё раз');
            }

            controls.ajax_action(action, method, succees, error, data);
        })
    },
    submit_file_init: function(form) {
        $('body').on('submit', form, function(e) {
            var form = this;
            var btn = $(form).find('button');
            var method = ($(form).attr('method') == undefined ? "POST" : $(form).attr('method'));
            var action = ($(form).attr('action') == undefined ? "" : $(form).attr('action'));
            e.preventDefault();
            var data = new FormData();

            jQuery.each(jQuery('input[type="file"]')[0].files, function(i, file) {
                data.append('import_file_'+i, file);
            });

            //$.each($('input[type="text"], select, textarea'), function(i, el) {
            //    var id = $(el).attr('name');
            //    var value = $(el).val();
            //    data.append(id, value);
            //});

            data.append('_token', csrf_token);

            $.ajax({
                url: action,
                method: method,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    alert.success('', 'Данные успешно сохранены');
                },
                error: function(data) {
                    alert.error('', 'Возникли ошибки');
                }
            })

        })
    }
}

controls = {
    //Send simple AJAX request
    ajax_action: function(action, method, success_function, error_function, data) {
        data += '&_token=' + csrf_token;
        $.ajax({
            url: action,
            method: method,
            data: data,
            success: function(data) {
                success_function(data);
            },
            error: function(data) {
                error_function(data.responseText);
            }
        })
    }
};

/**
 * User interface elements init
 * @type {{data_tables_init: Function, dependent_selects_init: Function, date_picker_init: Function}}
 */
ui = {
    data_tables_init: function() {
        $('.js_data_table').DataTable({
            "searching": false,
            "language": {
                "lengthMenu": "Показывать по _MENU_",
                "zeroRecords": "Пока ничего нет",
                "info": "Показано _PAGE_ из _PAGES_",
                "infoEmpty": "Ничего не найдено",
                "infoFiltered": "(filtered from _MAX_ total records)",
                paginate: {
                    first: "Первая",
                    previous: "Предыдущая",
                    next: "Следующая",
                    last: "Последняя"
                }
            }
        });
    },

    /**
     * Init select with dependencies possibility
     */
    dependent_selects_init: function() {
        $('body').on('change', '.js_depend_select', function() {

            var depended_select = $($(this).data('depend'));
            $(depended_select).html('');

            var data_src = $(this).data('src') + '/' + $(this).val();

            controls.ajax_action(data_src, 'GET',
                function(data) {
                    var options = $.parseJSON(data);
                    if (options.length > 0) {
                        options.forEach(function(item) {
                            $(depended_select).prepend('<option value="' + item.id + '">' + item.name + '</option>');
                        });
                    }
                },
                function(errors) {
                    console.log(errors);
                }
            )
        });
    },

    /**
     * Range data picker init
     */
    date_picker_init: function() {
        $('.js_date_range_picker').daterangepicker({
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "OK",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "по",
                "customRangeLabel": "Custom",
                "daysOfWeek": ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                "monthNames": ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                "firstDay": 1
            },
            "opens": "center",
            "autoUpdateInput": false
        }, function(start, end) {
            $('.js_date_range_picker').val('с ' + start.format('DD-MM-YYYY') + ' по ' + end.format('DD-MM-YYYY'));
            $('.js_start_date').val(start.format('DD-MM-YYYY'));
            $('.js_finish_date').val(end.format('DD-MM-YYYY'));
        });
    },

    /*Checkbox inputs init*/
    checkbox_init: function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-red'
        });
    },

    /*Init editor for textarea*/
    editor_init: function(el) {
        $(el).wysihtml5();
    },

    /*Collapse all box in form edit*/
    box_collapse: function() {
        $('.with-collapse').find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        $('.with-collapse').find('.box').addClass('collapsed-box');
    }
};

/*
Media library functions
 */

media = {
    /**
     * Select image for removing
     * @param btn
     */
    select: function(btn) {
        $('body').on('click', btn, function(event) {
            event.preventDefault();
            $(this).parent().parent().parent().toggleClass('__active');
            media.showDeleteSelected();
        });
    },
    /*
    Send request on server to delete image
     */
    delete: function(){
        var itemForDelete = [];
        $('body').find('.media-item.__acitve').each(function(i,el){
            itemForDelete.push($(this).data('id'));
        })
    },
    /*
     Init btn for 1 image delete
     */
    deleteBtnInit: function(btn){
        $('body').on('click', btn, function(e){
            e.preventDefault();
            var id = $(this).data('id');
            media.delete(id);
        })
    },
    /*
    Init button to delete all selected imges
     */
    deleteSelectedInit: function(btn){
        $('body').on('click', btn, function(e){
            e.preventDefault();
            var itemForDelete = [];
            $('body').find('.media-item.__acitve').each(function(i,el){
                itemForDelete.push($(this).data('id'));
            })
            media.delete(itemForDelete);
        })
    },
    /*
    Upload new images
    */
    uploadPreview: function(el) {
        $(el).change(function(event) {
            var files = this.files;
            var container = $(this).parent().find('.media-preview-l');
            container.html('');
            $.each(files, function(index, val) {
                var image = new FileReader();
                image.onload = function(e) {
                   container.append('<li class="media-preview-i"><img src="'+e.target.result+'" alt=""></li>')
                }
                image.readAsDataURL(val);
            });
        });
    },
    checkAllBtnInit: function(btn){
        $('body').on('click', btn, function(e){
            e.preventDefault();
            console.log('click');
            $('body').find('.media-item').addClass('__active');
            media.showDeleteSelected();
        })
    },
    showDeleteSelected: function(){
        if ($('.media-item.__active').length > 0) {
            $('.js-delete-selected').slideDown();
        } else {
            $('.js-delete-selected').slideUp();
        }
    }
}
